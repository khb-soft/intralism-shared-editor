# README #
Intralism Shared Editor for http://store.steampowered.com/app/513510/Intralism/

See TestEditor project for more info

Required http://www.newtonsoft.com/json (MIT license)

Project under CC BY-NC 4.0 (more info: https://creativecommons.org/licenses/by-nc/4.0/)