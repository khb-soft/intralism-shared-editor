﻿using IntralismSharedEditor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Выберите папку с картой");
            FolderBrowserDialog openFileDialog1 = new FolderBrowserDialog();
            openFileDialog1.ShowNewFolderButton = false;
            openFileDialog1.SelectedPath = Application.StartupPath;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string path = openFileDialog1.SelectedPath + "\\config.txt";
                string result = "";

                if (File.Exists(path))
                {
                    result = File.ReadAllText(path);
                }

                MapData data = CustomEditor.GetMap(result);
                Console.WriteLine(" ");
                Console.WriteLine("Name: " + data.name);
                Console.WriteLine("Music: " + openFileDialog1.SelectedPath + "\\" + data.musicFile);
                Console.WriteLine("Icon: " + openFileDialog1.SelectedPath + "\\" + data.iconFile);

                //Берем первый ивент для тестов
                MapEvent firstEvent = data.events[0];

                Console.WriteLine("First event: " + firstEvent.time + " : " + CustomEditor.GetEditorEventInfo(firstEvent).functionDescription); // Пишем время и описание
                
                Console.WriteLine("Parametrs: ");

                foreach (var par in CustomEditor.GetEditorEventInfo(firstEvent).parameters) // Идем по всем параметрам данного ивента
                {
                    Console.WriteLine(" - " + par.name);
                    CustomEventEditorHandler ceditor = CustomEditor.GetParametrEditor(par, firstEvent.data[1].Split(',')[CustomEditor.GetEditorEventInfo(firstEvent).parameters.IndexOf(par)]); // Создаем новый объект редактора параметра. В зависимости от типа, у объекта будут разные параметры для изменения. Например, для текста (IntralismSharedEditor.CustomEventEditorHandler_InputField) можно использовать ceditor.text
                    Console.WriteLine("   " + ceditor.GetType() + " " + ceditor.GetEditedData()); //GetEditedData() заново закодирует все параметры в string. Остается только перезаписать измененные параметры ивента firstEvent.data[1] = ceditor.GetEditedData();
                }
                Console.WriteLine(" ");
                Console.WriteLine("Result: " + CustomEditor.GetConfig(data)); // После всех изменений GetConfig() сгенерирует json-файл конфига.
            }
        }
    }
}
